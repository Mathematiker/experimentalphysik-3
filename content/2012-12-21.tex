% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 21.12.2012

Die bisherige Basis unserer optischen Betrachtungen, war auf die Wellenoptik, welche mittels der Maxwellgleichungen beschrieben werden kann, beschränkt. Jetzt wollen wir das Licht als >>Strahl<< näher untersuchen. 

Wir nehmen an, dass die zu untersuchenden Objekte viel größer sind als die Lichtwellenlänge. Hierbei werden Brechungseffekte vernachlässigt.

\begin{example*}[Kugelwelle]

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(4,1)
    \psdot(0,0)
    \psarc[](0,0){0.2}{-90}{90}
    \psarc[](0,0){0.4}{-90}{90}
    \psarc[](0,0){0.6}{-90}{90}
    \psarc[](0,0){0.8}{-90}{90}
    \psframe(1,0.1)(1.2,1)
    \psframe(1,-0.1)(1.2,-1)
    \psline[linecolor=DarkOrange3](1.3,0)(4,0.1)
    \psline[linecolor=DarkOrange3](1.3,0)(4,-0.1)
    \uput[90](3,0.1){\color{DarkOrange3} >>Lichtstrahl<<}
  \end{pspicture}
\end{figure}

\end{example*}

\begin{theorem*}[Definition eines >>Lichtstrahls<<:] ~

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(4,1)
    \psdot(0,0)
    \psarc[](0,0){0.2}{-25}{25}
    \psarc[](0,0){0.4}{-30}{30}
    \psarc[](0,0){0.6}{-35}{35}
    \psarc[](0,0){0.8}{-40}{40}
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0)(0.8;20)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0)(0.8;-20)
    \psline[linecolor=DarkOrange3]{->}(0.8;20)(1.4;20)
    \psline[linecolor=DarkOrange3]{->}(0.8;-20)(1.4;-20)
    \psarc(0.8;20){0.2}{20}{112}
    \psarc(0.8;-20){0.2}{-112}{-20}
    \psdot[dotscale=0.5](0.86;-26)
    \psdot[dotscale=0.5](0.86;26)
  \end{pspicture}
\end{figure}

\begin{enum-roman}
  \item Senkrechte (Normale) auf einer Wellenfront
  \item Ausbreitungsrichtung der Energie (Poyntingvektor)
  \item Wellenvektor $\bm{k}$ (in homogen, nicht brechenden Medien)
\end{enum-roman}
\end{theorem*}

\section{Das Fermat'sche Prinzip} 

\begin{figure}[H]
  \centering
  \begin{pspicture}(0,0)(4,1)
    \cnode*(0,0){2pt}{A}
    \cnode*(4,0){2pt}{B}
    \ncline[linecolor=DarkOrange3]{->}{A}{B}
    \nbput{\color{DarkOrange3} $S_0$}
    \ncarc[arcangle=20]{->}{A}{B}
    \ncarc[arcangle=40]{->}{A}{B}
    \uput[-90](A){\color{DimGray} $A$}
    \uput[-90](B){\color{DimGray} $B$}
  \end{pspicture}
\end{figure}

Die Lichtausbreitung erfolgt derart, dass der optisch Weg $W=nd$ (Produkt aus Brechungsindex und zurückgelegter Stercke) extremal wird auf den tatsächlich zurückgelegten Pfad $S_0$ gegenüber den benachbarten Pfaden $S_i$. Typischerweise wird der Lichtweg minimal (>>Licht ist faul<<). 

Für die optische Wellenlänge für den Pfad $S_0$ gilt:
\[
  W(s) = \int\limits_{S(A\to B)} n(\bm{x}) \; \mathrm{d}\bm{x}.
\]
Das \acct{Fermat'sche Prinzip} lautet also:
\[
  \frac{\delta W(s)}{\delta S} \bigg|_{S_0} = 0.
\]
Das $\delta$ verdeutlicht, dass ein Variationsproblem vorliegt. Das Fermat'sche Prinzip lässt sich formal aus den Maxwellgleichungen herleiten. 

\begin{notice*}[Vorstellung:]
Nur der >>wahre<< Lichtpfad führt zur phasenrichtigen Aufsummation (>>konstruktive Interferenz<<) aller Partialwellen am Detektor. Variation der Pfade führt zu einem zusätzlichen Weg und somit zu zusätzlicher Phase und damit destruktiver Interferenz. 
\end{notice*}

\begin{notice*}[Anwendung:]
Gradientenindex-Linse, Gradientenfaser

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-1)(4,1)
    \psframe(0,-1)(0.5,1)
    \psdot*[linecolor=DarkOrange3](1,0)
    \psline[linecolor=DarkOrange3](0.5,0.75)(1,0)
    \psline[linecolor=DarkOrange3](0.5,0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.5,0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.5,0)(1,0)
    \psline[linecolor=DarkOrange3](0.5,-0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.5,-0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.5,-0.75)(1,0)
    
    \psline[linecolor=DarkOrange3](0,0.75)(-1,0.75)
    \psline[linecolor=DarkOrange3](0,0.5)(-1,0.5)
    \psline[linecolor=DarkOrange3](0,0.25)(-1,0.25)
    \psline[linecolor=DarkOrange3](0,0)(-1,0)
    \psline[linecolor=DarkOrange3](0,-0.25)(-1,-0.25)
    \psline[linecolor=DarkOrange3](0,-0.5)(-1,-0.5)
    \psline[linecolor=DarkOrange3](0,-0.75)(-1,-0.75)
    
    \rput(2,-1){
      \psaxes[labels=none,ticks=none]{->}(0,0)(0,0)(1,2)[\color{DimGray} $n(r)$,0][\color{DimGray} $r$,0]
      \rput{-90}(0,1){
        \psplot[linecolor=MidnightBlue]{-0.8}{0.8}{0.5*2.7182818284590451^(-(x/0.3)^2)+0.2}
      }
    }
  \end{pspicture}
  
  \begin{pspicture}(-1,-1)(4,1)
    \pscurve(0.25,1)(0,0)(0.25,-1)
    \pscurve(0.25,1)(0.5,0)(0.25,-1)
    \psdot*[linecolor=DarkOrange3](1,0)
    \psline[linecolor=DarkOrange3](0.35,0.75)(1,0)
    \psline[linecolor=DarkOrange3](0.42,0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.48,0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.5,0)(1,0)
    \psline[linecolor=DarkOrange3](0.48,-0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.42,-0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.35,-0.75)(1,0)
    
    \psline[linecolor=DarkOrange3](0.15,0.75)(-1,0.75)
    \psline[linecolor=DarkOrange3](0.1,0.5)(-1,0.5)
    \psline[linecolor=DarkOrange3](0,0.25)(-1,0.25)
    \psline[linecolor=DarkOrange3](0,0)(-1,0)
    \psline[linecolor=DarkOrange3](0,-0.25)(-1,-0.25)
    \psline[linecolor=DarkOrange3](0.1,-0.5)(-1,-0.5)
    \psline[linecolor=DarkOrange3](0.15,-0.75)(-1,-0.75)
    
    \psline[linecolor=MidnightBlue](0.48,0.25)(0,0.25)
    \psline[linecolor=MidnightBlue](0.35,0.75)(0.15,0.75)
    
    \rput{0}(0.4,0.2){
      \pscurve[linecolor=MidnightBlue](0,0)(0.7,-0.1)(0.7,0.1)(1.4,0)
      \uput[0]{0}(1.4,0){\color{MidnightBlue} OwL $=n \cdot d$}
    }
  \end{pspicture}
  
  \begin{pspicture}(-1,-1)(4,1)
    \pscurve(0.25,1)(0,0)(0.25,-1)
    \pscurve(0.25,1)(0.5,0)(0.25,-1)
    \psdot*[linecolor=DarkOrange3](1,0)
    \psline[linecolor=DarkOrange3](0.35,0.75)(1,0)
    \psline[linecolor=DarkOrange3](0.42,0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.48,0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.5,0)(1,0)
    \psline[linecolor=DarkOrange3](0.48,-0.25)(1,0)
    \psline[linecolor=DarkOrange3](0.42,-0.5)(1,0)
    \psline[linecolor=DarkOrange3](0.35,-0.75)(1,0)
    
    \psline[linecolor=DarkOrange3](0.15,0.75)(-1,0.75)
    \psline[linecolor=DarkOrange3](0.1,0.5)(-1,0.5)
    \psline[linecolor=DarkOrange3](0,0.25)(-1,0.25)
    \psline[linecolor=DarkOrange3](0,0)(-1,0)
    \psline[linecolor=DarkOrange3](0,-0.25)(-1,-0.25)
    \psline[linecolor=DarkOrange3](0.1,-0.5)(-1,-0.5)
    \psline[linecolor=DarkOrange3](0.15,-0.75)(-1,-0.75)
    
    \psline(1,0.5)(3,0.5)
    \psline(1,-0.5)(3,-0.5)
    \psline(1,0.5)(1,-0.5)
    \psline(3,0.5)(3,-0.5)
    \psline(1,0.1)(3,0.1)
    \psline(1,-0.1)(3,-0.1)
    \uput[90](2,0.5){\color{DimGray} Glasfaser}
    
    \rput{180}(3.5,0){
      \psdot*[linecolor=DarkOrange3](0.5,0)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,0.75)(0,0.75)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,0.5)(0,0.5)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,0.25)(0,0.25)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,0.0)(0,0.0)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,-0.25)(0,-0.25)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,-0.5)(0,-0.5)
      \psbcurve[linecolor=DarkOrange3](0.5,0)l(0,0)r(0.5,-0.75)(0,-0.75)
      \psframe(0,-1)(0.5,1)
      \psline[linecolor=DarkOrange3](0,0.75)(-1,0.75)
      \psline[linecolor=DarkOrange3](0,0.5)(-1,0.5)
      \psline[linecolor=DarkOrange3](0,0.25)(-1,0.25)
      \psline[linecolor=DarkOrange3](0,0)(-1,0)
      \psline[linecolor=DarkOrange3](0,-0.25)(-1,-0.25)
      \psline[linecolor=DarkOrange3](0,-0.5)(-1,-0.5)
      \psline[linecolor=DarkOrange3](0,-0.75)(-1,-0.75)
    }
  \end{pspicture}
\end{figure}

\end{notice*}

\begin{notice*}[Anwendung:]
Transformationsoptik

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1.5)(2,1.5)
    \psline[linecolor=DarkOrange3]{o->}(-2,0)(-1.5,0)
    \psline[linecolor=DarkOrange3]{-o}(1.5,0)(2,0)
    \uput[-90](-2,0){\color{DarkOrange3} $A$}
    \uput[-90](2,0){\color{DarkOrange3} $B$}
    \rput(1.2;180){\color{DarkOrange3} ?}
    \pscircle(0,0){0.5}
    \pscircle(0,0){1}
    \pscircle(0,0){1.5}
    \rput(1.2;90){\color{DimGray}\scriptsize $n=1.4$}
    \rput(0.7;90){\color{DimGray}\scriptsize $n=1.3$}
    \rput(0.2;90){\color{DimGray}\scriptsize $n=1.2$}
  \end{pspicture}
\end{figure}

\end{notice*}

\minisec{Optische Tarnkappe}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1.5)(2,1.5)
    \psbcurve[linecolor=DarkOrange3](-2,0)l(-1,0)r(-1,0.6)(0,0.6)r(1,0)l(1,0.6)(2,0)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](-2,0)(2,0)
    \psdots*[linecolor=DarkOrange3](-2,0)(2,0)
    \uput[-90](-2,0){\color{DarkOrange3} $A$}
    \uput[-90](2,0){\color{DarkOrange3} $B$}
    \pscircle(0,0){0.5}
    \pscircle(0,0){1}
    \pscircle(0,0){1.5}
    \rput(1.2;90){\color{DimGray}\scriptsize $n=1.2$}
    \rput(0.7;90){\color{DimGray}\scriptsize $n=1.3$}
    \rput(0.2;90){\color{DimGray}\scriptsize $n=1.4$}
    \rput{0}(0.6,0.6){
      \pscurve[linecolor=DarkOrange3](0,0)(0.7,0.1)(0.7,-0.1)(1.4,0)
      \uput[0]{0}(1.4,0){\color{DarkOrange3} optisch kürzerer Weg}
    }
  \end{pspicture}
\end{figure}

\begin{notice*}[Trick:]
  Aus dem Fermat'schen Prinzip lässt sich ableiten, dass der Lichtweg im Prinzip umkehrbar ist (Absorption, Farady-Effekt im stationären Magnetfeld).
\end{notice*}

Das Extremum des optischen Weges kann ein Maximum oder Minimum sein (z.B.: beim elliptischen Spiegel). Das Gesetz der geradlinigen Lichtausbreitung im homogenen Medium folgt direkt aus dem Fermat'schen Prinzip.

\minisec{Reflexionsgesetz}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-1)(2,1.5)
    \psline(-2,0)(2,0)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](-2,0)(2,-0.2)
    \psline[linecolor=DarkOrange3](2;150)(0,0)
    \psline[linecolor=DarkOrange3](0,0)(2;30)
    \psline[linewidth=0.5\pslinewidth](2;150)(-1,0)(2;30)
    \psline[linewidth=0.5\pslinewidth](2;150)(1,0)(2;30)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0)(2;-30)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](2;-30)(2;30)
    \psdots[linecolor=DarkOrange3](2;30)(2;150)(2;-30)
    \psline[linestyle=dotted,dotsep=1pt](0,0)(0,1.5)
    \psarc[linecolor=MidnightBlue]{->}(0,0){0.8}{90}{150}
    \psarc[linecolor=MidnightBlue]{<-}(0,0){0.8}{30}{90}
    \uput{0.35}[120](0,0){\color{MidnightBlue} $\theta_{\text{ein}}$}
    \uput{0.35}[60](0,0){\color{MidnightBlue} $\theta_{\text{aus}}$}
    \uput[90](2;150){\color{DarkOrange3} $A$}
    \uput[90](2;30){\color{DarkOrange3} $B$}
    \uput[-90](2;-30){\color{DarkOrange3} $B'$}
    \uput[0](2,-0.1){\color{DimGray} Spiegel}
    \uput[-135](1;-30){\color{DarkOrange3} geradlinige Verbindung}
    \uput[180](-2,0.5){\color{MidnightBlue} $\theta_{\text{ein}} = -\theta_{\text{aus}}$}
  \end{pspicture}
\end{figure}


