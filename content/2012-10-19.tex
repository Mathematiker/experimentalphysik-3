% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 19.10.2012

Aus der Maxwellgleichung: $\div \bm{D}= \varrho$ folgt im Vakuum ($\varrho =  0$ --- keine freien Ladungen):
%
\begin{align*}
  \div \bm{E} &= 0 = \underbrace{\frac{\partial E_x}{\partial x} + \frac{\partial E_y}{\partial y}}_{\text{(a)}} + \frac{\partial E_z}{\partial z} 
\intertext{Für (a) gilt hierbei:}
  \text{(a)} &= \frac{\partial E_x}{\partial x} + \frac{\partial E_y}{\partial y} = 0
\end{align*}
%
Bei Ausbreitung in $z$-Richtung
\[
  E_z = \text{const.} .
\]
Elektromagnetische Wellen sind transversale Wellen im Vakuum.

\minisec{Komplexe Schreibweise}

Oft wird folgende komplexe Schreibweise verwendet:
%
\begin{subequations}
  \begin{align}
    \bm{E}_{+} &= \bm{E}_{0} \exp(\mathrm{i}(kz-\omega t)) \\
    \bm{E}_{-} &= \bm{E}_{0}^{\ast} \exp(-\mathrm{i}(kz-\omega t))
  \end{align}
\end{subequations}
%
Mit der Frequenz $\omega = \dfrac{2 \pi c}{\lambda}$.

Es ist zu beachten, dass nur der Realteil (z.B.: $\Re(\bm{E}_{x})$) oder der Imaginärteil (z.B.: $\Im(\bm{E}_{x})$) physikalisch sinnvolle Lösungen der Wellengleichung sind und gemessen werden können. Nun stellt sich die Frage nach dem Magnetfeld.

Aus den Maxwellgleichungen ergibt sich:
%
\begin{align}
  \nabla^2 \bm{B} &= \Delta \bm{B} = \frac{1}{c^2} \ddot{B}
\intertext{Dies ist die Wellengleichung für das magnetische Feld. Mit} 
  \rot \bm{E} &= - \dot{\bm{B}} \nonumber
\intertext{und Einsetzen der elektrischen Lösung}
  E_x &= E_{x_0} \exp(\mathrm{i}(kz-\omega t)) \nonumber
\intertext{erhält man}
  \frac{\partial}{\partial x} E_x &= - \dot{B} \nonumber .
\end{align}
%
Daraus folgt, dass $\bm{B} \perp \bm{E}$ ist.

Man sieht, dass beide Wellen das gleiche $\omega$ und $k$ haben. Um die Phasenverschiebung der beiden Felder zu betrachten nehmen wir wieder die ebene Wellenlösung und bilden die Zeitableitung:
%
\begin{align*}
  B_y &= B_{y_0} \exp(\mathrm{i}(kz-\omega t)) \\
  \dot{B}_y &= - \mathrm{i} \omega B_y
\intertext{Mit den Maxwellgleichungen erhalten wir}
  \frac{\partial}{\partial t} E_x &= \mathrm{i} k E_x = \mathrm{i} \omega  B_y .
\end{align*}
%
Daraus folgt:
%
\begin{align}
  E_x &= \underbrace{\frac{\omega}{k}}_{= c} B_y
\end{align}
%
Die beiden Wellen Schwingen im Vakuum in Phase:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-0.5)(6,1.3)
    \pstThreeDCoor[coorType=1,linecolor=DimGray,xMax=1,yMax=5,zMax=1,xMin=-0.5,yMin=-0.5,zMin=-0.5,nameX=\color{DimGray}$y$,nameY=\color{DimGray}$z$,nameZ=\color{DimGray}$x$]
    \parametricplotThreeD[xPlotpoints=200,linecolor=MidnightBlue,plotstyle=curve,algebraic](0,15.7){0.5*sin(t) | 0.3*t | 0}
    \parametricplotThreeD[xPlotpoints=200,linecolor=DarkOrange3,plotstyle=curve,algebraic](0,15.7){0 | 0.3*t | 0.5*sin(t)}
    \uput[0](4,0.7){\color{DarkOrange3} $\bm{E}$}
    \uput[0](4,-0.5){\color{MidnightBlue} $\bm{B}$}
  \end{pspicture}
\end{figure}

Die \acct{Intensität} bezeichnet die Energiestromdichte, also die transportierte Energie pro Zeit und Fläche, bzw. Leistung pro Fläche. Einheit: $\left[\frac{\mathrm{W}}{\mathrm{m^2}}\right]$
%
\begin{align}
  I &= \frac{\text{Energie}}{\text{Zeit} \cdot \text{Fläche}} = c \eta = c \varepsilon_0 E^2
\intertext{Die Energiedichte $\eta$ ist hierbei (mit $E = c B$):}
  \eta &= \frac{1}{2} \varepsilon_0 (E^2 + c^2 B^2) =  \varepsilon_0 E^2 
\end{align}
%
Der \acct{Poyntingvektor} erfasst die Richtungsabhängigkeit des Energietransports:
%
\begin{align}
  \bm{S} = \bm{E} \times \bm{H} \qquad \text{und} \qquad \bm{H} = \frac{1}{\mu_0} \bm{B}
\end{align}
%
Der Betrag des Poyntingvektors $\bm{S}$ entspricht genau der Intensität $I$:
%
\begin{align*}
  \boxed{|\bm{S}| = \varepsilon_0 \frac{1}{\mu_0 \varepsilon_0} |\bm{E}| |\bm{B}| = \varepsilon_0 c |E|^2 = I }
\end{align*}
%
Betrachten wir die Richtung des Poyntingvektors im Vakuum: 
\[
  \bm{S} \perp \bm{B} \text{   ,   } \bm{S} \perp \bm{E}
\]
Somit ist $\bm{S} \parallel \bm{k}$ im Vakuum.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,0)(2.5,2)
    \uput[180](-0.3,1){\parbox{7em}{\color{DimGray} \centering ebene \\ Wellenfronten}}
    \rput(0,0){\psline(0,0)(2;100)}
    \rput(0.5,0){\psline(0,0)(2;100)}
    \rput(1,0){\psline(0,0)(2;100)}
    \rput(1.5,0){\psline(0,0)(2;100)}
    \rput(0.5,0){\rput(1.5;100){
      \psline[linecolor=DarkOrange3]{->}(0,0)(2;10)
      \psarc[linecolor=DarkOrange3]{->}(0,0){0.3}{-80}{10}
      \psdot*[linecolor=DarkOrange3,dotscale=0.4](0.15;-35)
      \uput[-90](2;10){\color{DarkOrange3} $\bm{S}$}
    }}
    \rput(0.5,0){\rput(0.5;100){
      \psline[linecolor=DarkOrange3]{->}(0,0)(2;10)
      \psarc[linecolor=DarkOrange3]{->}(0,0){0.3}{-80}{10}
      \psdot*[linecolor=DarkOrange3,dotscale=0.4](0.15;-35)
      \uput[-90](2;10){\color{DarkOrange3} $\bm{k}$}
    }}
  \end{pspicture}
\end{figure}

Der Poyntingvektor steht senkrecht auf der Wellenfront und ist somit parallel zu k.
\[
  \bm{E} = \bm{E}_0 \exp(\mathrm{i}(\omega t - \bm{k} \cdot \bm{r}))
\]
$\bm{k}$ ist hierbei der Wellenvektor mit der Wellenzahl: $|\bm{k}| = \dfrac{2 \pi}{\lambda}$

\subsection{Interferenz}

\begin{theorem*}[Superpositionsprinip]
  Überlagerung von Lösungen der Wellengleichung sind wieder eine Lösung der Wellengleichung.
\end{theorem*}

Bei gegebenen Randbedingungen lässt sich durch Linearkombination von bekannten Lösungen (z.B.: ebenen Wellen) eine passende Lösung für das Problem finden.

\begin{example*} Eine Welle wird an einem Metallspiegel in sich zurück reflektiert (bei $z=0$).
Die Randbedingung ist somit $\bm{E}_{\text{ges}}(z=0)=0$:
%
\begin{align*}
  \bm{E}_{\text{hin}}(z,t) &= \bm{E}_0 \cos(\omega t + k z) \\
  \bm{E}_{\text{rück}}(z,t) &= -\bm{E}_0 \cos(\omega t - k z)
  \intertext{Diese Gleichungen erfüllen die Randbedingung:}
  0 &= E_{\text{hin}}(z=0) + E_{\text{rück}}(z=0)
  \intertext{Durch Addition erhalten wir die Lösung:}
  \bm{E}_{\text{ges}} &= \bm{E}_{0} \left[  \cos(\omega t - k z) - \cos(\omega t + k z) \right] \\
    &= 2 \bm{E}_0 \sin(kz) \sin(\omega t)
\end{align*}
%
\end{example*}
