% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 7.12.2012

\subsection{Reflexions- und Brechungsindex}

Die Herleitung erfolgt über die Stetigkeit von Normalkomponente und $\bm{D}$ und $\bm{B}$ und Tangentialkomponente von \bm{E} und \bm{H} an der Grenzfläche (aus Maxwelldifferentialgleichung mit Randbedingunen). Die Stetigkeit führt zu:
%
\begin{align*}
  E_{0,ex} \cos(\theta_e) + E_{0,rx} \cos(\theta_r) &= E_{0,tx} \cos(\theta_t) \\
  E_{0,ez} \cos(\theta_e) + E_{0,rz} \cos(\theta_r) &= E_{0,tz} \cos(\theta_t)
\end{align*}
%
Für alle $\bm{r}$ mit $y=0$ und für alle $t$ ist
%
\begin{align*}
  \omega_e t - \bm{k}_e \bm{r} &= \omega_r t - \bm{k}_r \bm{r} = \omega_t t - \bm{k}_t \bm{r} . 
\end{align*}
%
Die Frequenz ist hierbei immer konstant:
\[
  \omega_e = \omega_r = \omega_t = \omega
\]
Weiterhin muss für $y=0$ gelten:
% 
\begin{align*}
  \bm{k}_e \bm{r} &= \bm{k}_r \bm{r} &\implies& &(\bm{k}_e-\bm{k}_r)\bm{r} = 0 \\
  \bm{k}_e \bm{r} &= \bm{k}_t \bm{r} &\implies& &(\bm{k}_e-\bm{k}_t)\bm{r} = 0 
\end{align*}
%
Die Komponenten von $\bm{k}_e$ und $\bm{k}_r$, die parallel zur Grenzfläche liegen, sind gleich
\[
  \bm{k}_{eG} = \bm{k}_{rG} .
\]
Da $\omega_e = \omega_r$ ist und $k_e = k_r = \frac{\omega n_e}{c}$ folgt:
%
\begin{align*}
  k_{eG} &= \frac{\omega n_e}{c} \sin(\theta_e)  \\
   &= k_{rG} = \frac{\omega n_e}{c} \sin(r) \\
  \implies \sin(\theta_e) &= \sin(\theta_r)
\end{align*}
%
\begin{theorem*}[Reflexionsgesetz]
\begin{enum-roman}
  \item Der Wellenvektor des reflektierenden Lichtes ist in der Einfallsebene.
  \item Der Ausfallwinkel $\theta_r$ ist gleich dem Einfallwinkel $\theta_e$:
  \[
    \theta_r = \theta_e
  \] 
  (mathematisch sauber eigentlich $\theta_r = -\theta_e$).
  
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-1.5,0)(1.5,1)
      \psline(-1,0)(1,0)
      \psline(0,0)(0,1)
      \psline[linecolor=MidnightBlue]{->}(0,0)(1.5;30)
      \psline[linecolor=MidnightBlue]{<-}(0,0)(1.5;150)
      
      \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{90}{150}
      \psarc[linecolor=DarkOrange3]{<-}(0,0){0.7}{30}{90}
      
      \uput{0.3}[120](0,0){\color{DarkOrange3} $\theta_e$}
      \uput{0.3}[60](0,0){\color{DarkOrange3} $\theta_r$}
      
      \uput[30](1.5;30){\color{MidnightBlue} $k_e$}
      \uput[150](1.5;150){\color{MidnightBlue} $k_r$}
    \end{pspicture}
  \end{figure}
\end{enum-roman}
\end{theorem*}
Analog für einen transmitierten Strahl gilt:
\[
  k_{eG} = k_{tG}.
\]
Es folgt also:
\[
  k_{eG} = \frac{\omega n_e}{c} \sin(\theta_e) = k_{tG} = \frac{\omega n_t}{c} \sin(\theta_t).
\]
Daraus ergibt sich das so genannte \acct{Snelliussche Brechungsgesetz}:
\[
  \boxed{n_e \sin(\theta_e) = n_t \sin(\theta_t)}
\]
Die Stetigkeit der Tangentialkomponente von $E$ führt direkt zur Erhaltungsgröße $n \sin(\theta)$.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-4)(4,1)
    \psline(-1,0)(3,0)
    \psline(-1,-1)(3,-1)
    \psline(-1,-2)(3,-2)
    \psline(-1,-3)(3,-3)
    \psline(0,-4)(0,1)
    \psline[linecolor=MidnightBlue]{<-}(0,0)(1.5;150)
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{90}{150}
    \uput{0.3}[120](0,0){\color{DarkOrange3} $\theta_e$}
    \rput(0,0){
      \psline[linecolor=Purple]{->}(0,0)(1.064;-70)
      \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{-90}{-70}
      \psline[linestyle=dotted,dotsep=1pt](0,0)(0,-0.8)
      \uput{0.3}[-110](0,0){\color{DarkOrange3} $\theta_1$}
      \rput(1.064;-70){
        \psline[linecolor=Purple]{->}(0,0)(1.556;-40)
        \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{-90}{-40}
        \psline[linestyle=dotted,dotsep=1pt](0,0)(0,-0.8)
        \uput{0.3}[-65](0,0){\color{DarkOrange3} $\theta_2$}
        \rput(1.556;-40){
          \psline[linecolor=Purple]{->}(0,0)(1.155;-60)
          \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{-90}{-60}
          \psline[linestyle=dotted,dotsep=1pt](0,0)(0,-0.8)
          \uput{0.3}[-110](0,0){\color{DarkOrange3} $\theta_3$}
          \rput(1.155;-60){
            \psline[linecolor=MidnightBlue]{->}(0,0)(1.0;-30)
            \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{-90}{-30}
            \psline[linestyle=dotted,dotsep=1pt](0,0)(0,-0.8)
            \uput{0.2}[-100](0,0){\color{DarkOrange3} $\theta_{\text{Substrat}}$}
          }
        }
      }
    }
    \uput[0](3,0.5){\color{DimGray} $n_e$}
    \uput[0](3,-0.5){\color{DimGray} $n_1$}
    \uput[0](3,-1.5){\color{DimGray} $n_2$}
    \uput[0](3,-2.5){\color{DimGray} $n_3$}
    \uput[0](3,-3.5){\color{DimGray} $n_{\text{Substrat}}$}
  \end{pspicture}
\end{figure}

\[
  n_e \sin \theta_e = n_1 \sin \theta_1 = n_2 \sin \theta_2 = n_3 \sin \theta_3 = n_{\text{Substrat}} \sin \theta_{\text{Substrat}}
\]

\begin{notice*}
Es zeigt sich also, dass für die Berechnung des Ausfallwinkels (im Bild $\theta_{\text{Substrat}}$) nur der Brechungsindex $n_e$ (einfallend) und $n_{\text{Substrat}}$ von Bedeutung ist, alle anderen Brechungindize dienen lediglich zur >>Verschiebung<< des Strahles.
\end{notice*}

Der Übergang vom optisch dünneren ins optisch dichtere Medium sieht wie folgt aus:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1.5)(1.5,1.5)
    \psline[linecolor=MidnightBlue]{->}(1.5;135)(0,0)
    \psline[linecolor=MidnightBlue]{->}(-1.0605,0)(0,0)
    \psline[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](-1.0605,0)(1.5;135)
    \psarc[linecolor=MidnightBlue]{->}(0,0){1}{90}{135}
    \uput{0.6}[112.5](0,0){\color{MidnightBlue} $\theta_e$}
    \uput[-90](-1,0){\color{MidnightBlue} $k_{eG}$}
    \uput[135](1.5;135){\color{MidnightBlue} $k_{e}$}
    
    \psline[linecolor=DarkOrange3]{->}(0,0)(1.5;-60)
    \psline[linecolor=DarkOrange3]{->}(0,0)(0.75,0)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0.75,0)(1.5;-60)
    \psarc[linecolor=DarkOrange3]{->}(0,0){1}{-90}{-60}
    \uput{0.6}[-75](0,0){\color{DarkOrange3} $\theta_t$}
    \uput[90](0.5,0){\color{DarkOrange3} $k_{tG}$}
    \uput[-60](1.5;-60){\color{DarkOrange3} $k_{t}$}
    
    \uput[0](1.5,1){\color{MidnightBlue} $n_e = 1$}
    \uput[0](1.5,-1){\color{DarkOrange3} $n_t = 1.5$}
  \end{pspicture}
\end{figure}

Der Übergang vom optisch dünneren ins optisch dichtere Medium sieht wie folgt aus:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1.5)(1.5,1.5)
    \psline[linecolor=MidnightBlue]{->}(1.5;135)(0,0)
    \psline[linecolor=MidnightBlue]{->}(-1.0605,0)(0,0)
    \psline[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](-1.0605,0)(1.5;135)
    \psarc[linecolor=MidnightBlue]{->}(0,0){1}{90}{135}
    \uput{0.6}[112.5](0,0){\color{MidnightBlue} $\theta_e$}
    \uput[-90](-1,0){\color{MidnightBlue} $k_{eG}$}
    \uput[135](1.5;135){\color{MidnightBlue} $k_{e}$}
    
    \psline[linecolor=DarkOrange3]{->}(0,0)(1.5;-30)
    \psline[linecolor=DarkOrange3]{->}(0,0)(1.299,0)
    \psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](1.299,0)(1.5;-30)
    \psarc[linecolor=DarkOrange3]{->}(0,0){1}{-90}{-30}
    \uput{0.6}[-60](0,0){\color{DarkOrange3} $\theta_t$}
    \uput[90](1,0){\color{DarkOrange3} $k_{tG}$}
    \uput[-30](1.5;-60){\color{DarkOrange3} $k_{t}$}
    
    \uput[0](1.5,1){\color{MidnightBlue} $n_e = 1.5$}
    \uput[0](1.5,-1){\color{DarkOrange3} $n_t = 1$}
  \end{pspicture}
\end{figure}

\subsection{Fresnel'sche Formeln für den Reflexionsgrad einer Grenzfläche}
\index{Fresnel'sche Formeln für den Reflexionsgrad einer Grenzfläche}

Betrachtet wird der Fall senkrecht einfallenden Lichtes.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-1)(1,1)
    \psline(-1,0)(1,0)
    \uput[0](1,0.5){\color{DimGray} $n_e$}
    \uput[0](1,-0.5){\color{DimGray} $n_t$}
    
    \psline[linecolor=DarkOrange3]{->}(0,0)(0,1)
    \uput[0](0,0.5){\color{DarkOrange3} $k_r$}
    
    \psline[linecolor=MidnightBlue]{->}(0,0)(0,-1)
    \uput[0](0,-0.5){\color{MidnightBlue} $k_t$}
    
    \psline{->}(-0.5,1)(-0.5,0)
    \uput[180](-0.5,0.5){\color{DimGray} $k_e$}
  \end{pspicture}
\end{figure}

\begin{align*}
  \bm{E}_{0,e} + \bm{E}_{0,r} &= \bm{E}_{0,t} & \bm{B}_0 &= \frac{1}{\omega} \left(\bm{k} \times \bm{E} \right) \\
  \bm{B}_{0,e} + \bm{B}_{0,r} &= \bm{B}_{0,t} & \bm{k}_r &= - \bm{k}_e \\
  n_e \bm{E}_{0,e} + n_e \bm{E}_{0,r} &= n_e \bm{E}_{0,t} & \bm{k}_t &= \frac{n_t}{n_e} \bm{k}_e 
\end{align*}
%
Aus diesen Gleichungen folgt:
\[
  \bm{E}_{0,r} = \underbrace{\frac{n_e-n_t}{n_e + n_t}}_{= r} \bm{E}_{0,e} = r \bm{E}_{0,e}.
\]
$r$ beschreibt hierbei den Amplituden-Reflexionskoeffizienten, der wie folgt definiert ist:
\[
  r = \frac{n_e-n_t}{n_e + n_t}
\]
Weiterhin folgt:
\[
  \bm{E}_{0,t} = t \bm{E}_{0,e}.
\]
$t$ beschreibt hierbei den Amplituden-Transmissionskoeffizienten, der wie folgt definiert ist:
\[
  t = \frac{2 n_e}{n_e + n_t}
\]

Fällt Licht vom optisch dichteren Medium auf eine Grenzfläche zum optisch dünneren Medium $(n_e > n_t)$, so ist der komplexe Amplituden-Reflexionskoeffizient $r>0$ und $\bm{E}_e$ und $\bm{E}_r$ sind in Phase $(\Delta \varphi = 0)$, es findet also kein Phasensprung statt. Dies bezeichnet man auch als \acct{Reflexion am losen Ende}.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,0)(4,1)
    \rput(0,0){
      \psplot[plotpoints=200,linecolor=MidnightBlue]{-1}{1}{2.7182818284590451^(-(x/0.2)^2)}
      \psline{->}(0.5,0.5)(1.0,0.5)
    }
    \rput(3,0){
      \psplot[plotpoints=200,linecolor=MidnightBlue]{-1}{1}{2.7182818284590451^(-(x/0.2)^2)}
      \psline{->}(-0.5,0.5)(-1.0,0.5)
    }
  \end{pspicture}
\end{figure}

Fällt Licht vom optisch dünneren Medium auf eine Grenzfläche des optisch dichteren Medium, so ist $n_e<n_t$ und wegen $r = \frac{n_e - n_t}{n_r + n_t}$ ist $r$ negativ! Das heißt, dass beide Felder gegenphasig sind, es erfolgt also ein Phasensprung von $\pi \widehat{=} 180^\circ$. Man spricht auch von einer \acct{Reflexion am festen Ende}.

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,0)(4,1)
    \rput(0,0){
      \psplot[plotpoints=200,linecolor=MidnightBlue]{-1}{1}{2.7182818284590451^(-(x/0.2)^2)}
      \psline{->}(0.5,0.5)(1.0,0.5)
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](1,-0.1)(1.2,1.1)
      \psline(1,-0.1)(1,1.1)
    }
    \rput(3,1){
      \psplot[plotpoints=200,linecolor=MidnightBlue]{-1}{1}{-2.7182818284590451^(-(x/0.2)^2)}
      \psline{->}(-0.5,-0.5)(-1.0,-0.5)
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](1,0.1)(1.2,-1.1)
      \psline(1,0.1)(1,-1.1)
    }
  \end{pspicture}
\end{figure}

Für den Reflexionsgrad gilt:
%
\begin{align*}
  R &= \frac{I_r}{I_e} = r \cdot r^\ast \\
  &= |{r}|^2 \\
  &= \left| \frac{n_e-n_t}{n_e + n_t}\right|^2
\end{align*}
%
\begin{example*}
  Betrachtet wird die Grenzfläche Luft-Glas ($n=1.5$) bei senkrechten Lichteinfall:
  %
  \begin{align*}
    R &= \left|\frac{1-1.5}{1+1.5}\right|^2 \\
    &= \left|0.2\right|^2 \\
    &= 0.04
  \end{align*}
  %
  Die Rechnung zeigt, dass $4$ \% der Intensität ($20$ \% der Amplitude des einfallenden $E$-Feldes) reflektiert werden.
  Es zeigt sich also folgendes Problem. Bei Objektiven mit vielen Linsen (z.T. bis zu $10$) haben wir bis zu $20$ Grenzflächen. D.h.
  \[
    I_{\text{ges}} = I_e (1-0.04)^{20} = 0.44
  \] 
  Ohne Antireflexionsbeschichtung (>>Vergütung<<) kommt nur $44$ \% des einfallenden Lichtes durch.
\end{example*}

\subsection{Reflexion von polarisierenden Licht}

Bei p-polarisierten Licht handelt es sich um Licht, bei dem der $E$-Feldvektor parallel zur Einfallsebene ist (aus Lot zur Grenzfläche und Einfallsvektor).

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.5,-1.5)(1.5,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-1.5)(1.5,1.5)
    
    \uput[180](-1.5,0.5){\color{DimGray} $n_e$}
    \uput[180](-1.5,-0.5){\color{DimGray} $n_t$}
    
    \psline[linecolor=MidnightBlue](0,0)(1.5;30)
    \psline[linecolor=MidnightBlue](0,0)(1.5;150)
    \psline[linecolor=MidnightBlue](0,0)(1.5;-60)
    
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.7}{90}{150}
    \psarc[linecolor=DarkOrange3]{<-}(0,0){0.7}{30}{90}
    \psarc[linecolor=DarkOrange3]{->}(0,0){0.9}{-90}{-60}
    
    \uput{0.3}[120](0,0){\color{DarkOrange3} $\theta_e$}
    \uput{0.3}[60](0,0){\color{DarkOrange3} $\theta_r$}
    \uput{0.5}[-75](0,0){\color{DarkOrange3} $\theta_t$}
    
    \rput{120}(1.2;30){
      \psline{<->}(-0.3,0)(0.3,0)
      \uput[-45]{-120}(0.3,0){\color{DimGray} $\bm{E}_r$}
      \pscircle[linecolor=DarkRed](0,0){0.1}
      \uput{0.2}[-135]{-120}(0,0){\color{DarkRed} $\bm{B}_r$}
    }
    
    \rput{60}(1.2;150){
      \psline{<->}(-0.3,0)(0.3,0)
      \uput[-45]{-60}(0.3,0){\color{DimGray} $\bm{E}_e$}
      \pscircle[linecolor=DarkRed](0,0){0.1}
      \uput{0.2}[-135]{-60}(0,0){\color{DarkRed} $\bm{B}_e$}
    }
    
    \rput{30}(1.2;-60){
      \psline{<->}(-0.3,0)(0.3,0)
      \uput[-45]{-30}(0.3,0){\color{DimGray} $\bm{E}_t$}
      \pscircle[linecolor=DarkRed](0,0){0.1}
      \uput{0.2}[-135]{-30}(0,0){\color{DarkRed} $\bm{B}_t$}
    }
  \end{pspicture}
\end{figure}

Die Komponentenerhaltung ergibt:
%
\begin{align*}
  E_{e,T} &= E_e \cos(\theta_e) & E_{e,N} &= E_e \sin(\theta_e) \\
  E_{t,T} &= E_e \cos(\theta_t) & E_{t,N} &= E_e \sin(\theta_t) \\
  E_{r,T} &= E_e \cos(\theta_r) & E_{r,N} &= E_e \sin(\theta_r) 
\end{align*}
%
Die Stetigkeit für
\[
E_{e,T} + E_{r,T} = E_{t,T}
\]
liefert:
\[
  \cos(\theta_e) (E_e - E_r) = E_t \cos(\theta_t)
\]

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-0.5)(1,1)
    \psline[linestyle=dotted,dotsep=1pt](-1,0)(1,0)
    \psline{->}(1;135)(1;-45)
    \psline{->}(0,0)(1;45)
    \psline[linecolor=MidnightBlue](0,0)(0.707,0)
    \psline[linecolor=DarkOrange3](0.707,0)(1;45)
    \uput[-135](0,0){\color{DimGray} $\bm{k}_e$}
    \uput[135](0.5;45){\color{DimGray} $\bm{E}_e$}
    \uput[-45](0.5,0){\color{MidnightBlue} $\bm{E}_{eN}$}
    \uput[0](0.707,0.5){\color{DarkOrange3} $\bm{E}_{eT}$}
  \end{pspicture}
\end{figure}

Die Stetigkeit für $D_N$ (Normalkomponente) zeigt:
%
\begin{align*}
  \varepsilon &= n^2 \\ 
  \bm{D} &= \varepsilon \bm{E} = n^2 \bm{E} \\
  &\implies n_e^2 E_{e,N} + n_e^2 E_{t,N} = n_t^2 E_{t,N} \\
  &\implies n_e^2 \sin(\theta_e) (E_e + E_r) = n_t^2 E_t \sin(\theta_t) \\
  &\implies \frac{n_e}{n_t} (E_e + E_r) = E_t  
\end{align*}
%
Es folgt für den Amplituden-Reflexionskoeffizienten bei p-polarisierten Licht:
\[
  r_P = \frac{E_r - n_t \cos(\theta_e) - n_e \cos(\theta_t)}{E_e - n_t \cos(\theta_e) + n_e \cos(\theta_t)}
\]
