% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 13.11.2012

\minisec{Zur Interpretation der Wellenfunktion $\psi$}

\begin{notice*}[Beobachtung:]
  Wellenpakete zerfließen!
\end{notice*}

Für ein freies Teilchen mit kinetischer Energie $E_{\text{kin}}$ gilt:
\[
   E_{\text{kin}} = \frac{p^2}{2 m} = \frac{\hbar^2 k^2}{2 m} = \hbar \omega
\]
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(1.5,2)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(1.5,2)[\color{DimGray} $k$,0][\color{DimGray} $w(k)$,180]
    \psplot[linecolor=DarkOrange3,yMaxValue=2]{0}{1.5}{x^2}
  \end{pspicture}
\end{figure}
%
Daraus folgt
\[
\frac{\partial^2 \omega }{\partial k^2} = \frac{\hbar}{m} \neq 0 .
\]
Dies ist die zweite Ableitung der Dispersionsrelation. Es zeigt sich, dass die Krümmung reziprok zur Masse ist, also:
\[
  \text{Krümmung} \propto \frac{1}{\text{Masse}}
\]
Das heißt, dass Objekte mit großer Masse langsamer >>zerfließen<<.

\begin{example*}
  Ein Elektron mit Ortsschärfe von $\Delta x(t=0) = 1 \r{A}$. Da 
  %
  \begin{align*}
    \Delta x \cdot \Delta p \geq \hbar \\
    \Delta x  \cdot m  \cdot \Delta v \geq \hbar 
  \end{align*}
  %
  hat nach nur $10 \mu\mathrm{s}$ das Elektron eine Ortsunschärfe von $10$ m.
\end{example*}

Im Experiment misst man aber nur punktförmige Teilchen. Nach der >>Kopenhagener Interpretation der Quantenmechanik<< gibt die Größe 
\[
  \psi(x,t) \psi^\dagger(x,t) \; \mathrm{d}x = |\psi(x,t)|^2 \; \mathrm{d}x 
\]
der >>Elektronenwelle<< die Wahrscheinlichkeitsdichte an, ein Teilchen zum Zeitpunkt $t$ im Intervall $[x,x+\mathrm{d}x]$ zu finden. Es gelten folgende Eigenschaften:
\begin{item-triangle}
  \item $|\psi|^2$ ist nicht direkt messbar, da es eine Wahrscheinlichkeitsdichte beschreibt. Erst für viele gleich präparierte Teilchen ist es messbar (Ähnlich wie $I \propto |E|^2$ bei elektromagnetischen Wellen.).
  
  \item $\psi$ eines einzelnen Teilchens ist nicht messbar im Gegensatz zum elektrischen Feld.
  
  \item $\psi$ beschreibt was wir über ein Teilchen wissen.
  
  \item Da das Teilchen irgendwo im Raum sein muss, gilt 
  \[
    1 = \int\limits_{-\infty}^{\infty} |\psi(x,t)|^2 \; \mathrm{d}V \qquad \text{Normierungsbedingung}
  \]
  
  \item Daraus folgt, dass physikalisch sinnvolle Lösungen quadratintegrabel sein müssen, d.h. dass das Integral des Betragsquadrates von $-\infty$ bis $\infty$ ein endlicher Wert sein muss, damit die Funktion normierbar ist.
  
  \item $|\psi|$ hat die Einheit:
  %
  \begin{align*}
    \text{eindimensional:} & &[|\psi|^2] &= \frac{1}{m} \implies [\psi] = \frac{1}{\sqrt{m}} \\
    \text{dreidimensional:} & &[|\psi|^2] &= \frac{1}{m^3} \implies [\psi] = \frac{1}{{m}^{3/2}} 
  \end{align*}
  
  \item $|\psi|^2$ ist eine Wahrscheinlichkeitsdichte
  
  \item $|\psi|^2 \; \mathrm{d}V$ ist eine Wahrscheinlichkeit
\end{item-triangle} 

\minisec{Doppelspaltexperimente}

Wird von einer Punktquelle ausgegangen, wobei mit Kugeln (z.B.: Schrot) das Experiment durchzuführen ist, erhält man folgende Anordnung: 
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(6,1)
    \psdot(0,0)
    \psline{->}(0.1;30)(0.8;30)
    \psline{->}(0.1;15)(0.8;15)
    \psline{->}(0.1;0)(0.8;0)
    \psline{->}(0.1;-15)(0.8;-15)
    \psline{->}(0.1;-30)(0.8;-30)
    \psframe(1,0.2)(1.2,1)
    \psframe(1,0.1)(1.2,-0.1)
    \psframe(1,-0.2)(1.2,-1)
    \uput[30](1.2,0.15){\color{DimGray} $1$}
    \uput[-30](1.2,-0.15){\color{DimGray} $2$}
    \rput{-90}(3,0){
      \psline(-1.1,0)(1.1,0)
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=0.3]{-1}{1}
      \uput[10]{90}(0.1,1){\color{MidnightBlue} $P_1$}
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=-0.3]{-1}{1}
      \uput[170]{90}(-0.1,1){\color{MidnightBlue} $P_2$}
    }
    \rput{-90}(5,0){
      \psline(-1.1,0)(1.1,0)
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.4,mue=0]{-1}{1}
      \uput[90]{90}(0,0.5){\color{MidnightBlue} $P_{12}$}
    }
  \end{pspicture}
\end{figure}
%
Wird von einer Punktquelle ausgegangen, wobei mit Licht/Wellen das Experiment durchzuführen ist, erhält man folgende Anordnung: 
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(6,1)
    \psdot(0,0)
    \psarc[](0,0){0.2}{-20}{20}
    \psarc[](0,0){0.4}{-25}{25}
    \psarc[](0,0){0.6}{-30}{30}
    \psarc[](0,0){0.8}{-35}{35}
    \psframe(1,0.2)(1.2,1)
    \psframe(1,0.1)(1.2,-0.1)
    \psframe(1,-0.2)(1.2,-1)
    \uput[30](1.2,0.15){\color{DimGray} $1$}
    \uput[-30](1.2,-0.15){\color{DimGray} $2$}
    \rput{-90}(3,0){
      \psline(-1.1,0)(1.1,0)
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=0.3]{-1}{1}
      \uput[10]{90}(0.1,1){\color{MidnightBlue} $P_1$}
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=-0.3]{-1}{1}
      \uput[170]{90}(-0.1,1){\color{MidnightBlue} $P_2$}
    }
    \rput{-90}(5,0){
      \psline(-1.1,0)(1.1,0)
      \psplot[linecolor=MidnightBlue]{-1}{1}{(sin(10*x)^2)/(100*x^2)}
      \uput[90]{90}(0,1){\color{MidnightBlue} $P_{12}$}
    }
  \end{pspicture}
\end{figure}
%
Wird von einer Punktquelle ausgegangen, wobei mit Elektronen das Experiment durchzuführen ist, erhält man folgende Anordnung: 
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(0,-1)(6,1)
    \psdot(0,0)
    \psline{->}(0.1;30)(0.8;30)
    \psline{->}(0.1;15)(0.8;15)
    \psline{->}(0.1;0)(0.8;0)
    \psline{->}(0.1;-15)(0.8;-15)
    \psline{->}(0.1;-30)(0.8;-30)
    \psframe(1,0.2)(1.2,1)
    \psframe(1,0.1)(1.2,-0.1)
    \psframe(1,-0.2)(1.2,-1)
    \uput[30](1.2,0.15){\color{DimGray} $1$}
    \uput[-30](1.2,-0.15){\color{DimGray} $2$}
    \rput{-90}(3,0){
      \psline(-1.1,0)(1.1,0)
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=0.3]{-1}{1}
      \uput[10]{90}(0.1,1){\color{MidnightBlue} $P_1$}
      \psGauss[linecolor=MidnightBlue,unit=0.5cm,sigma=0.2,mue=-0.3]{-1}{1}
      \uput[170]{90}(-0.1,1){\color{MidnightBlue} $P_2$}
    }
    \rput{-90}(5,0){
      \psline(-1.1,0)(1.1,0)
      \psplot[linecolor=MidnightBlue]{-1}{1}{(sin(10*x)^2)/(100*x^2)}
      \uput[90]{90}(0,1){\color{MidnightBlue} $P_{12}$}
    }
  \end{pspicture}
\end{figure}
%
Elektronen werden als einzelne Teilchen am Detektor gemessen. Bei vielen Elektronen ergibt sich das Interferenzmuster.

\minisec{Mittelwert, Wahrscheinlichkeit, Fluktuationen}

Für den \acct{Mittelwert} gilt:
%
\begin{align*}
  w_1 &= \{a,b\} \\
   \implies \langle w_1 \rangle &= \frac{a+b}{2} \\
  w_2 &= \{a,a,b,a,c,d,c,a,b,d,d,c,d,a,a\}  \\
  \implies \langle w_2 \rangle &= \frac{6a+2b+3c+4d}{15} \\
  &= \frac{6}{15}a + \frac{2}{15}b + \frac{3}{15}c + \frac{4}{15}d
\end{align*}
%
Die Brüche (z.B.: $6/15$) bezeichnen die \acct{Wahrscheinlichkeit} für die jeweilige Variable. Allgemein gilt die Formel:
%
\begin{align*}
  \langle w \rangle &= \Pi(a) a + \Pi(b) b +\Pi(c) c +\Pi(d) d
\end{align*}
%
$\Pi$ beschreibt hierbei die jeweilige Wahrscheinlichkeit einer Variablen.
%
\begin{align*}
  w &= \{ a_i : i=1\ldots n, \Pi(a_i)  \} \\
  \implies \langle w \rangle &= \sum_{i=1}^{n} \Pi(a_i) a_i 
\end{align*}
%
Es ist klar, dass immer gelten muss;
\[
\sum_{i=1}^{n} \Pi(a_i) = 1
\]

\minisec{Elektron im Kastenpotential}

Gehen wir zur Quantenmechanik und betrachten ein \acct{Elektron im Kastenpotential}:
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.2,-0.2)(1.2,2)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](-1.1,0)(-1,2)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](1,0)(1.1,2)
    \psline(-1.2,0)(1.2,0)
    \psline(-1,-0.2)(-1,2)
    \psline(1,-0.2)(1,2)
    \psline{<->}(-1,-0.2)(1,-0.2)
    \uput[-90](-1,-0.1){\color{DimGray} $0$}
    \uput[-90](1,-0.1){\color{DimGray} $a$}
    \rput*(0,-0.3){\color{DimGray} $\ell$}
    \psplot[linecolor=MidnightBlue]{-1}{1}{0.2*cos(1.57079632679*x)}
    \psplot[linecolor=MidnightBlue]{-1}{1}{0.2*sin(3.14159265359*x)+0.7}
    \psplot[linecolor=MidnightBlue]{-1}{1}{-0.2*cos(4.71238898038*x)+1.4}
  \end{pspicture}
\end{figure}
%
Für das Teilchen im Potentialkasten gilt folgendes Potential:
\[
  E_{\text{pot}} = \begin{cases} 0, & \text{für $0 \leq x_0 \leq a$} \\ \infty, & \text{sonst}  \end{cases}
\]
Im Gebiet  $0 \leq x_0 \leq a$ gilt für ein freies Teilchen $E_{\text{pot}} = 0$.
\[
  \frac{\mathrm{d}^2}{\mathrm{d}x^2} \psi + k^2 \psi = 0 \; , \quad k^2 = \frac{2,E}{\hbar^2}
\]
Die allgemeine Lösung lautet:
\[
  \psi = A \exp(\mathrm{i}kx) + B \exp(-\mathrm{i}kx)
\]
Das Teilchen, kann die Bereiche $x < 0$ und $ x > a $ nicht erreichen, d.h.
%
\begin{align*}
  \psi(0) &= 0 && A+B=0 \\
  \psi(a) &= 0 && A \exp(\mathrm{i}ka) + B \exp(-\mathrm{i}ka) = 0 \\
  \implies \psi &= A (\exp(\mathrm{i}ka)-\exp(-\mathrm{i}ka)) \\
  &= 2\mathrm{i} A \sin(kx) \\
  &= 2 \mathrm{i}A\sin\left(\frac{n \pi }{a} x\right) \\
  \implies ka &= n \pi && n\in\mathbb{N} \\
  \implies \psi_n(x) &= 2 \mathrm{i} A \sin\left(\frac{n \pi }{a} x\right) \\
  &= c \sin\left(\frac{n \pi }{a} x\right) \\
  \implies \lambda_n &= \frac{2a}{n} \\
                 k_n &= \frac{n \pi }{a}
\end{align*}
%
Die Schrödingergleichung liefert:
\[
  \psi(x,t) = \sqrt{\frac{2}{L}} \sin\left(\frac{\pi x}{L}\right) \exp\left(-\frac{\mathrm{i} E t}{\hbar}\right)
\]
Der Term $\sqrt{{2}/{L}}$ bezeichnet hierbei die \acct[0]{Normierungskonstamte}.
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.2,-0.2)(1.2,1)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](-1.1,0)(-1,1)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](1,0)(1.1,1)
    \psline(-1.2,0)(1.2,0)
    \psline(-1,-0.2)(-1,1)
    \psline(1,-0.2)(1,1)
    \uput[90](-0.8,0){\color{DimGray} $1$}
    \uput[90](0,0){\color{DimGray} $\Delta x$}
    \uput[90](0.8,0){\color{DimGray} $N$}
    \multido{\rx=-1.0+0.1}{20}{%
      \psline(\rx,0)(\rx,0.1)
    }
  \end{pspicture}
\end{figure}
%
Die Wahrscheinlichkeit das Elektron in der $j$-ten Box zu finden ist gegeben durch:
\[
  \Pi(j) = \underbrace{\psi^\ast(x_j,t) \psi(x_j,t)}_{=|\psi_j|^2}
\]
Somit erhält man für den Mittelwert der Position:
\[
  \langle x \rangle = \sum_{j=1}^{N} \Pi(j)x_j = \sum_{j=1}^{N} \psi_j^\ast x_j \psi_j \cdot \Delta x
\]
Im Fall für $\lim\limits_{N \to \infty}$ folgt:
\[
  \langle x \rangle = \int \psi^\ast(x,t) \, x \, \psi(x,t) \; \mathrm{d}x 
\]
Wichtig ist, dass im Allgemeinem gilt:
\[
  \langle x \rangle^2 \neq \langle x^2 \rangle
\]
Der Mittelwert des Impulses ist gegeben durch:
\[
  \langle \hat{p} \rangle = \int \psi^\ast(x,t) (-\mathrm{i}\hbar \partial_x) \psi(x,t) \; \mathrm{d}x
\]
Dabei ist die Positionierung des Differentialoperators wichtig:
%
\begin{align*}
  \int \psi^\ast(x,t) \hat{p} \psi(x,t) \; \mathrm{d}x &\neq \int \hat{p} \psi^\ast(x,t) \psi(x,t) \; \mathrm{d}x \\
  \langle \hat{p} \rangle &= \int \psi^\ast (-\mathrm{i} \hbar \partial_x) \psi \; \mathrm{d}x \\
  &= \ldots = 0
\end{align*}
%
Allgemein gilt für den Erwartungswert eines Operators:
\[
  \langle \hat{A} \rangle = \int \psi^\ast(x,t) \hat{A} \psi(x,t) \; \mathrm{d}x
\]

\minisec{Fluktuationen am Beispiel eines Würfels} \index{Fluktuationen}

Bei einem Würfel treten die Zahlen $\{1,2,3,4,5,6\}$ mit gleicher Wahrscheinlichkeit auf. Der Erwartungswert beträgt hierbei:
\[
  \langle \alpha \rangle = \frac{1}{6} \sum_{i} \alpha_i = 3.5
\]

\begin{notice*}[Frage:]
  Wie groß ist die Wahrscheinlichkeit für eine Abweichung vom Mittelwert?
\end{notice*}

Hier ist der Mittelwert eine ganze Zahl, da der Würfel nur die Zahlen $1,2,3,4,5,6$ liefert, d.h. der Mittelwert wird nie gemessen. Somit ist die Wahrscheinlichkeit für eine Abweichung $P=1$.

\begin{notice*}[Frage:]
  Um welchen Wert wird im Mittel der gemessene Wert vom Mittelwert abweichen?
\end{notice*}

\[
  \langle \delta_\alpha \rangle = \langle \alpha - \langle \alpha \rangle \rangle = \langle \alpha - 3.5 \rangle = \langle \alpha \rangle - \langle 3.5 \rangle = 0
\]
Somit sieht man, dass dieser Mittelwert immer für symmetrische Wahrscheinlichkeitsverteilungen verschwindet. 

\acct{Varianz} als zweites Mittel der Verteilung:
%
\begin{align*}
  \langle (\delta_\alpha)^2 \rangle &= \langle (\alpha - \langle \alpha \rangle)^2 \rangle \\
  &= \langle \alpha^2 - 2\alpha \langle \alpha \rangle + \langle \alpha \rangle^2 \rangle \\
  &= \langle \alpha^2 \rangle - \langle 2 \alpha \langle \alpha \rangle \rangle +\langle \langle \alpha \rangle^2 \rangle \\
  &= \langle \alpha^2 \rangle - 2 \langle \alpha \rangle  \langle \alpha \rangle  +\langle \alpha \rangle^2 \\
  &= \langle \alpha^2 \rangle - \langle \alpha \rangle^2 \\
\end{align*}
%
Hierbei beschreibt der linke Wert den >>Mittelwert der Quadrate<< und der rechte Wert den >>Mittelwert der verschiedenen Werte<<. Für den Würfel gilt:
\[
  \langle (\delta_\alpha)^2 \rangle = \frac{35}{12} \approx 2.9
\]
In der Quantenmechanik ist der Wert, der der Wurzel aus der Varianz entspricht, die sogenannte \acct{Ortsunschärfe}
\[
  \Delta x = \sqrt{\langle (\delta_\alpha)^2\rangle} .
\]
