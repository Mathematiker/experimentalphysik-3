% Henri Menke, Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 16.11.2012

\begin{example*}
Berechne $\Delta x$ für $\psi_2$ im Kastenpotential: 
%
\begin{align*}
  \langle (\delta_x)^2 \rangle &= \langle x^2 \rangle - \langle x \rangle^2 \\
  &= L^2 \left(\frac{1}{2} - \frac{1}{2}\frac{1}{(2 \pi)^2}\right) - \left(\frac{L}{2}\right)^2 \\
  &= L^2 \left(\frac{1}{12} - \frac{1}{8 \pi^2}\right) \\
  \Delta x &= \sqrt{\langle (\delta_x)^2 \rangle} \\
  &= L \sqrt{\frac{1}{12} - \frac{1}{8 \pi^2}} 
\end{align*}
%
Genauso kann man die Impulsunschärfe $\Delta p$ für $\psi_2$ berechnen: 
%
\begin{align*}
  \langle (\delta_p)^2 \rangle &= \langle p^2 \rangle - \langle p \rangle^2 \\
  &= \langle p^2 \rangle -0 \\
  &= \int \psi_2^\ast (x,t) (-\mathrm{i}\hbar \partial_x)^2 \psi_2(x,t) \; \mathrm{d}x \\
  &= \left(\frac{\hbar 2 \pi }{L}\right)^2 \\
  \Delta p &= \sqrt{(\delta_x)^2} = \frac{2 \pi \hbar }{L}
\end{align*}
%
Nimmt man das Produkt 
\[
  \Delta x_{\psi_2} \cdot \Delta p_{\psi_2} = \frac{\hbar}{2} \sqrt{\frac{4 \pi^2}{3} - 2} \geq \frac{\hbar}{2}
\]
%
\end{example*}

Die \acct{Heisenbergsche Unschärferelation} lautet dementsprechend wie folgt:
%
\begin{align}
  \boxed{\Delta x \cdot \Delta p \geq \frac{\hbar}{2}}
\end{align}
%
Mit dieser Relation kann man zum Beispiel die Größe eines Atomkerns abschätzen:
\[
  \Delta x \cdot \Delta p \approx \hbar
\]
Für die Bindungsenergie eines Photons im Kern kann von $\approx 1 \mathrm{MeV} = 10^6 \, \mathrm{eV}$ ausgegangen werden.
\[
  E = \frac{\langle p^2 \rangle}{2 m } \approx \frac{(\Delta p)^2}{2 m} \implies \Delta p = \sqrt{2 m E}
\]
mit $\Delta x \approx \frac{\hbar}{\Delta p}$ folgt durch einsetzen:
\[
  \Delta x \approx 5 \cdot 10^{-15} \, \mathrm{m} \approx 5 \, \mathrm{fm}
\] 
Dies entspricht ungefähr der Größenordnung der Kerngröße.

\minisec{Vertauschungsrelationen}
Man definiert den \acct[0]{antisymmetrischen Kommutator}\index{Kommutator!antisymmetrisch}:

Für den \acct[0]{fermionischen Kommutator}\index{Kommutator!fermionisch}, der nur für Teilchen mit halbzahligem Spin gilt, folgt:
\[
  [\hat{A},\hat{B}] = [\hat{A},\hat{B}]_{-} = \hat{A}\hat{B} - \hat{B}\hat{A} 
\] 
Für den \acct[0]{bosonischen Kommutator}\index{Kommutator!bosonisch}, der nur für Teilchen mit ganzzahligen Spin gilt (Photonen, Cooper-Paare), folgt:
\[
  [\hat{A},\hat{B}] = [\hat{A},\hat{B}]_{+} = \hat{A}\hat{B} + \hat{B}\hat{A} 
\]
$\hat{A}$ und $\hat{B}$ sind hierbei Operatoren.

Atome können ganzzahligen Gesamtspin besitzen, das bedeutet, dass sogenannte >>Bose-Einstein-Kondensate<< möglich sind:
\begin{item-triangle}
  \item Boson $^4_2\mathrm{He}$ ($2n+2p$)
  \item Fermion $^3_2\mathrm{He}$ ($2p +1n$)
\end{item-triangle}
Im Allgemeinen ist der fermionische (antisymmetrische) Kommutator $[\hat{A},\hat{B}] \neq 0$

\begin{example*}
  Drehung eines Quaders im Raum um die $x$- und $y$-Achse um $90^\circ$ kommutieren nicht.
\end{example*}  

\begin{example*} Der Kommutator von $\hat{x}$ und $\hat{p}$.
%
\begin{align}
  [\hat{p},\hat{x}] \psi &= -\mathrm{i} \hbar \partial_x x \psi + x \mathrm{i} \hbar \psi  \nonumber\\
  &= -\mathrm{i} \hbar (\psi + x \partial_x \psi) + x \mathrm{i} \hbar \partial_x \psi \nonumber\\
  &= - \mathrm{i} \hbar \psi \nonumber \\
  \implies &\boxed{[\hat{p},\hat{x}] = -\mathrm{i} \hbar} \\
  &\boxed{[\hat{x},\hat{p}] = \mathrm{i} \hbar}
\end{align}
%
\end{example*}

Setzt man dies in die Heisenbergsche Unschärferelation ein, so erhält man:
%
\begin{align}
  \Delta x \cdot \Delta p &\geq \left|\frac{[\hat{p},\hat{x}]}{2} \right| 
\end{align}
%
Dies lässt sich auf zwei Operatoren $\hat{A}$ und $\hat{B}$ verallgemeinern:
%
\begin{align}
  \Delta A \cdot \Delta B &\geq \left|\frac{[\hat{A},\hat{B}]}{2} \right| 
\end{align}
%
\minisec{Allgemein gilt:}

Wenn zwei Operatoren nicht miteinander vertauschen (also deren antisymmetrischer Kommutator nicht Null ist), dann können beide nicht gleichzeitig scharf gemessen werden (also können $\Delta x$ und $\Delta p$, bzw. $\Delta A$ und $\Delta B$ nicht gleichzeitig Null sein).
